;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  ISR multiplexes LED display implementations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			.cdecls C,LIST,"msp430.h"       ; Include device header file

; Extern variables

			.ref	win
			.ref	lose
			.ref	curr_comb
			.ref	index
			.ref	display_on
			.ref	start

; Decoding symbols table
			.sect	.const
segtab		.byte	0x77					; symbols table
			.byte	0xFF
			.byte	0x4E
			.byte	0x7E
			.byte	0x4F
			.byte	0x47

; TIMERA0 Interrupt service routine

			.text
CCR0ISR 	push.w	R12						; push R12, R11 on stack
			push.w	R11
			cmp.b	#0,start
			jeq		OFF_ALL
			cmp.b	#1,win					; if game over all displays turn off
			jeq		OFF_ALL
			cmp.b 	#1,lose
			jeq 	OFF_ALL
DISP1		cmp.b 	#0x00,display_on		; turn on display1
			jne 	DISP2
			mov.w 	#curr_comb,R12
			mov.b	@R12,R11
			bis.b 	#BIT6,&P10OUT
			mov.b 	segtab(R11),&P6OUT
			bic.b 	#BIT1,&P11OUT
			jmp 	NEW_CURR
DISP2		cmp.b 	#0x01,display_on		; turn on display2
			jne 	DISP3
			mov.w 	#curr_comb,R12
			add.w	#1,R12
			mov.b	@R12,R11
			cmp.b 	#1,index
			jge 	ON2
			bis.b 	#BIT1,&P11OUT
			mov.b 	#0x00,&P6OUT
			bic.b 	#BIT0,&P11OUT
			jmp 	NEW_CURR
ON2		    bis.b 	#BIT1,&P11OUT
			mov.b 	segtab(R11),&P6OUT
			bic.b	#BIT0,&P11OUT
			jmp 	NEW_CURR
DISP3		cmp.b 	#0x02,display_on		; turn on display3
			jne 	DISP4
			mov.w 	#curr_comb,R12
			add.w	#2,R12
			mov.b	@R12,R11
			cmp.b 	#2,index
			jge 	ON3
			bis.b 	#BIT0,&P11OUT
			mov.b 	#0x00,&P6OUT
			bic.b 	#BIT7,&P10OUT
			jmp 	NEW_CURR
ON3		    bis.b 	#BIT0,&P11OUT
			mov.b 	segtab(R11),&P6OUT
			bic.b 	#BIT7,&P10OUT
			jmp 	NEW_CURR
DISP4 		cmp.b 	#0x03,display_on		; turn on display4
			jne 	NEW_CURR
			mov.w 	#curr_comb,R12
			add.w	#3,R12
			mov.b	@R12,R11
			cmp.b 	#3,index
			jeq	 	ON4
			bis.b 	#BIT7,&P10OUT
			mov.b 	#0x00,&P6OUT
			bic.b 	#BIT6,&P10OUT
			jmp 	NEW_CURR
ON4 	    bis.b 	#BIT7,&P10OUT
			mov.b 	segtab(R11),&P6OUT
			bic.b 	#BIT6,P10OUT
			jmp 	NEW_CURR
NEW_CURR	add.b 	#0x01,display_on
			cmp.b 	#0x04,display_on
			jne 	END
			mov.b	#0x00,display_on
			jmp 	END
OFF_ALL 	bis.b 	#BIT1,&P11OUT
			bis.b 	#BIT0,&P11OUT
			bis.b 	#BIT7,&P10OUT
			bis.b 	#BIT6,&P10OUT
END 		pop.w 	R11						; return R11,R12 from stack
			pop.w	R12
			reti

; Vectors
			.sect	.int54
			.short	CCR0ISR
