# Mastermind game

In Mastermind game(Skocko), player has to guess a variation with repetiton of 6 symbols, taken 4 times in 6 attempts.
Symbols in game: A, B, C, D, E, F. Current combination is show on LED display, results shows on UART console  

- Microcontroller: MSP438F5538A
- Software: Code Composer Studio 9.3.0 
- Peripheral: 4 × buttons, 4 × LED display, UART Console

