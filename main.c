/**
 * @file main.c
 * @brief pm160207-projekat
 *
 * Projekat broj 25
 *
 * In this project is realized game Skocko.
 * In this game, player has to guess a variation with repetiton of 6 symbols, taken 4 times in 6 attempts.
 * Symbols in game: A, B, C, D, E, F
 * Current combination is show on LED display, results shows on UART console.
 * Buttons configuration:
 *  S1 - up
 *  S2 - down
 *  S3 - enter
 *  S4 - reset/start
 *
 * @date 2020
 * @author Milovan Puric 0207-2016
 *
 * @version [2.1 @ 09.2020] Game start when we press start button
 * @version [2.0 @ 09/2020] ISR multiplexes LED display, written in asmembly
 * @version [1.0 @ 09/2020] Initial version
 */
#include <msp430.h>
#include <stdint.h>
#include <stdlib.h>

/**
 * @brief Timer period
 *
 * Timer is clocked by ACLK (32768Hz).
 * We want ~2ms period, so use 63 for CCR0
 */
#define TIMER_PERIOD        (1048) /* ~32ms (31.25ms) */

/** macros for uars messeges length  */
#define MESS1_LENG  (44)
#define MESS2_LENG  (6)
#define MESS3_LENG  (25)

/** baud rate */
#define BR38600     (27)

/** macro to convert digit to ASCII code */
#define DIGIT2ASCII(x)      (x + '0')

/** macro to convert number to symbol */
#define NUMBER2SYM(x)      (x + 'A')

/** variable where store current player's combination */
volatile uint8_t curr_comb[4] = {0};

/** variable where store randomly generated combination */
volatile uint8_t goal_comb[4] = {0};

/**
 * Table of settings for a-g lines for appropriate symbol
 */
/*const uint8_t segtab[] = {0x77, 0xFF, 0x4E, 0x7E, 0x4F, 0x47}; */

/** variable used for display multiplex */
volatile uint8_t display_on = 0;

/** variable indicates which button started timer  */
volatile uint8_t button = 0;

/** variable that shows current symbol in combination */
volatile uint8_t index = 0;

/** variable that shows number of attempts */
volatile uint8_t tried = 0;

/** number of correct symbols on right place */
volatile uint8_t good_sym = 0;

/** number of correct symbols on wrong place */
volatile uint8_t bad_place = 0;

/** flag used to show that chosen combination is correct */
volatile uint8_t win = 0;

/** flag used to show that we can't find right combination in 6 attempts */
volatile uint8_t lose = 0;

/** flag used to start game on */
volatile uint8_t start = 0;

/** flag used to signal that we can show result of current combination */
volatile uint8_t entry_done = 0;

/** flag used to signal that we press S3 */
volatile uint8_t s3_press = 0;

/** flag used to signal that we press S4 */
volatile uint8_t s4_press = 0;

/* messages transmit on UART, shows current state of the game */
volatile uint8_t message1[MESS1_LENG] = {'P', 'o', 'k', 'u', 's', 'a', 'j', ' ', '0', ':', ' ',
                                        'A', 'A', 'A', 'A', ' ', '0', ' ', 'c', 'o', 'r', 'r', 'e', 'c', 't', ' ',
                                        '0', ' ', 'o', 'n', ' ', 'g','o', 'o', 'd', ' ', 'p', 'l', 'a', 'c', 'e', '.', 0xA, 0xD};
volatile uint8_t message2[MESS2_LENG] = {'W', 'I', 'N', '!', 0xA, 0xD};
volatile uint8_t message3[MESS3_LENG] = {'L', 'O', 'S', 'E', '!', 0xA, 0xD, 'R', 'I', 'G', 'H', 'T', ' ', 'C', 'O', 'M', 'B', ':', ' ', 'A', 'A', 'A', 'A', 0xA, 0xD};

//** counters used during receive and transmit for different messages */
volatile int8_t data_cnt1 = 0;
volatile int8_t data_cnt2 = MESS2_LENG;
volatile int8_t data_cnt3 = MESS3_LENG;


/**
 * @brief Main function
 *
 * Demo initializes buttons, LED displays, Timer A and UART TX interrupt
 */
int main(void) {

	WDTCTL = WDTPW | WDTHOLD; // stop watchdog timer
	
	/* configure S1-S4 */
	P2DIR &= ~0xF0; // set port P2.7-P2.4 as in
	P2IES |= 0xF0; // interrupt on high to low transition
	P2IFG &= ~0xF0;             // clear the flag initially
	P2IE  |= 0xF0;              // enable interrupt on P2.7-P2.4


	/* initialize 7seg LED */
    P11DIR |= BIT1 | BIT0; // set P10.0-P10.1 to out
	P10DIR |= BIT7 | BIT6; // set P10.6-P10.7 to out
	P6OUT &= ~0x7F; // clear P6.6-P6.0
	P6DIR |= 0x7F; // set P6.6-P6.0 to out
	/* turn off all displays */
	P11OUT |= BIT1 | BIT0;
	P10OUT |= BIT7 | BIT6;

	/* initialize Timer A*/
	TA0CCR0 = TIMER_PERIOD;
	TA0CCTL0 = CCIE; // enable CCR0 interrupt
	TA0CTL = TASSEL__ACLK | TAIE | MC__UP | MC_2;

	/* initialize UART */
	P3SEL |= BIT4 | BIT5; // enable P3.4 and P3.5 for UART

	UCA0CTL1 |= UCSWRST; // set software reset

	UCA0CTL1 |= UCSSEL__SMCLK | UCSWRST; // use SMCLK
	UCA0BRW = BR38600; // BR = 27
	UCA0MCTL = UCBRS_2; // BRS = 2 for 38400bps

	UCA0CTL1 &= ~UCSWRST; // release software reset

	UCA0IFG = 0; // clear IFG
	UCA0IE |= UCTXIE; // enable tx interrupts

	__enable_interrupt();

	while(1) {
	    if (entry_done == 1) {
	        UCA0TXBUF = message1[data_cnt1];
	        data_cnt1++;
	        entry_done = 0;
	        index = 0;
	        curr_comb[0] = 0;
	        curr_comb[1] = 0;
	        curr_comb[2] = 0;
	        curr_comb[3] = 0;
	    }
	    if (s3_press == 1) {
	        if (!((start == 0) || (win == 1) || (lose == 1))) {
	            switch (index){
	            case 0:
	                message1[11] = NUMBER2SYM(curr_comb[index]);
	                index++;
	                break;
	            case 1:
	                message1[12] = NUMBER2SYM(curr_comb[index]);
	                index++;
	                break;
	            case 2:
	                message1[13] = NUMBER2SYM(curr_comb[index]);
	                index++;
	                break;
	            case 3:
	                message1[14] = NUMBER2SYM(curr_comb[index]);
	                tried++;
	                good_sym = 0;
	                bad_place = 0;
	                uint8_t gs_gp[4] = {0};
	                uint8_t gs_bp[4] = {0};
	                uint8_t i, j;
	                for (i = 0; i < 4; i++) {
	                    gs_gp[i] = ((curr_comb[i] == goal_comb[i]) ? 1 : 0);
	                    good_sym += gs_gp[i];
	                }
	                if (good_sym == 4) {
	                    win = 1;
	                    data_cnt2 = 0;
	                }
	                else {
	                    for (i = 0; i < 4; i++) {
	                        if (gs_gp[i] == 0) {
	                            for (j = 0; j < 4; j++) {
	                                if (gs_gp[j] == 0) {
	                                    if (gs_bp[j] == 0) {
	                                        if (curr_comb[i] == goal_comb[j]) {
	                                            gs_bp[j] = 1;
	                                            bad_place++;
	                                            break;
	                                        }
	                                    }
	                                }
	                            }
	                         }
	                     }
	                     if (tried == 6) {
	                        lose = 1;
	                        data_cnt3 = 0;
	                        message3[19] = NUMBER2SYM(goal_comb[0]);
	                        message3[20] = NUMBER2SYM(goal_comb[1]);
	                        message3[21] = NUMBER2SYM(goal_comb[2]);
	                        message3[22] = NUMBER2SYM(goal_comb[3]);
	                     }
	                }
	                message1[8] = DIGIT2ASCII(tried);
	                message1[16] = DIGIT2ASCII(bad_place + good_sym);
	                message1[26] = DIGIT2ASCII(good_sym);
	                data_cnt1 = 0;
	                entry_done = 1;
	            }
	        }
	        s3_press = 0;
	    }
	    if (s4_press == 1) {
	        uint8_t k;
	        for (k = 0; k < 4; k++) {
	            curr_comb[k] = 0;
	            goal_comb[k] = rand() % 6;
	        }
	        start = 1;
	        index = 0; tried = 0;
	        win = 0; lose = 0;
	        s4_press = 0;
	    }
	}
}


/**
 * @brief PORT2 Interrupt service routine
 *
 * ISR blocks P2 interrupt, and enables timer for debounce counting.
 */
void __attribute__ ((interrupt(PORT2_VECTOR))) P2ISR (void) {
    if (button == 0) {
        if ((P2IFG & BIT7) != 0) {
            P2IE &= ~BIT7;          // disable P2.7 interrupt
            button = 4;
            TA1CTL |= MC__UP;       // enable Timer A1 in UP mode
            P2IFG &= ~BIT7;         // clear interrupt flag
        }
        if ((P2IFG & BIT6) != 0) {
            P2IE &= ~BIT6;          // disable P2.6 interrupt
            button = 3;
            TA1CTL |= MC__UP;       // enable Timer A1 in UP mode
            P2IFG &= ~BIT6;         // clear interrupt flag
        }
        if ((P2IFG & BIT5) != 0) {
            P2IE &= ~BIT5;          // disable P2.5 interrupt
            button = 2;
            TA1CTL |= MC__UP;       // enable Timer A1 in UP mode
            P2IFG &= ~BIT5;         // clear interrupt flag
        }
        if ((P2IFG & BIT4) != 0) {
            P2IE &= ~BIT4;          // disable P2.4 interrupt
            button = 1;
            TA1CTL |= MC__UP;       // enable Timer A1 in UP mode
            P2IFG &= ~BIT4;         // clear interrupt flag
        }
    }
    return;
}

/**
 * @brief TIMERA1 Interrupt service routine
 *
 * ISR debounces P2.7-P2.4
 */
void __attribute__ ((interrupt(TIMER0_A1_VECTOR))) TACC0ISR (void) {
    switch (button) {
    case 1:
        if ((P2IN & BIT4) == 0) {
            // S1 - UP is pressed
            if (!((start == 0) || (win == 1) || (lose == 1))) {
                if (curr_comb[index] == 5) {
                    curr_comb[index] = 0;
                }
                else {
                    curr_comb[index]++;
                }
            }
        }
        TA0CTL &= ~MC__UPDOWN;      // stop the timer
        TA0CTL |= TACLR;            // clear the timer
        P2IE |= BIT4;               // enable P2.4 interrupt
        button = 0;
        break;
    case 2:
        if ((P2IN & BIT5) == 0) {
            // S2 - DOWN is pressed
            if (!((start == 0) || (win == 1) || (lose == 1))) {
                if (curr_comb[index] == 0) {
                    curr_comb[index] = 5;
                }
                else {
                    curr_comb[index]--;
                }
            }
        }
        TA0CTL &= ~MC__UPDOWN;      // stop the timer
        TA0CTL |= TACLR;            // clear the timer
        P2IE |= BIT5;               // enable P2.5 interrupt
        button = 0;
        break;
    case 3:
        if ((P2IN & BIT6) == 0) {
            /*  S3 - ENTER is pressed
             * With this button we confirm symbol what we choose with up/down buttons.
             * And when we confirm all 4 symbols, we compare our combination with given one.
             * After every combination we increment number of attempts, and control if we came at the end of game. */
            s3_press = 1;
        }
        TA0CTL &= ~MC__UPDOWN;      // stop the timer
        TA0CTL |= TACLR;            // clear the timer
        P2IE |= BIT6;               // enable P2.6 interrupt
        button = 0;
        break;
    case 4:
        if ((P2IN & BIT7) == 0) {
            /** S4 - RESET/START is pressed
             * With this button we return game to begin, randomly generated new combination and number of attempts set on 0. */
            s4_press = 1;
        }
        TA0CTL &= ~MC__UPDOWN;      // stop the timer
        TA0CTL |= TACLR;            // clear the timer
        P2IE |= BIT7;               // enable P2.7 interrupt
        button = 0;
        break;
    }
    return;
}

/**
 * @brief TIMERA0 Interrupt service routine
 *
 * ISR multiplexes LED display
 * But only display that shows, active symbol or symbol what we alredy choose is turn on.
 */
/*
void __attribute__ ((interrupt(TIMER0_A0_VECTOR))) CCR0ISR (void) {
    static uint8_t current = 0;
    if (!((win == 1) || (lose == 1))) {
        switch (current) {
        case 0:
            P10OUT |= BIT6;
            P6OUT = segtab[curr_comb[0]];
            P11OUT &= ~BIT1;
            break;
        case 1:
            P11OUT |= BIT1;
            P6OUT = ((index>=1) ? segtab[curr_comb[1]] : 0x00);
            P11OUT &= ~BIT0;
            break;
         case 2:
            P11OUT |= BIT0;
            P6OUT = ((index>=2) ? segtab[curr_comb[2]] : 0x00);
            P10OUT &= ~BIT7;
            break;
         case 3:
            P10OUT |= BIT7;
            P6OUT = ((index>=3) ? segtab[curr_comb[3]] : 0x00);
            P10OUT &= ~BIT6;
            break;
         }
         current = (current + 1) & 3;
    }
    else {
        turn off all displays
        P11OUT |= BIT1 | BIT0;
        P10OUT |= BIT7 | BIT6;
    }

}*/
/**
 * @brief USCIA0 ISR
 *
 * Implement packet transfers
 */
void __attribute__ ((interrupt(USCI_A0_VECTOR))) UARTISR (void) {
    if (UCA0IV == USCI_UCTXIFG) {
            if (data_cnt1 < MESS1_LENG) {
                UCA0TXBUF = message1[data_cnt1];
                data_cnt1++;
            }
            else {
                data_cnt1 = MESS1_LENG;
                if (data_cnt2 < MESS2_LENG) {
                    UCA0TXBUF = message2[data_cnt2];
                    data_cnt2++;
                }
                else {
                    data_cnt2 = MESS2_LENG;
                    if (data_cnt3 < MESS3_LENG) {
                        UCA0TXBUF = message3[data_cnt3];
                        data_cnt3++;
                    }
                    else {
                        data_cnt3 = MESS3_LENG;
                    }
                }
            }
    }
}
